﻿using System;
using System.Timers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace SignalRConsole
{
    class Program
    {
        private Timer aTimer;

        private HubConnection connection; 

        private string deviceId; 
        
        static void Main(string[] args)
        {
            Console.Write("Enter unique Id: ");
            string deviceId = Console.ReadLine();

            Program program = new Program(deviceId);
            Task task = program.SetupPollRequests();
            Task.WaitAll(task);

            Console.WriteLine("Waiting...");
            Console.ReadLine();

            Task.WaitAll(program.Shutdown());
        }

        public Program(string deviceId) { 
            this.deviceId = deviceId;
        }

        public async Task Shutdown() { 
            try { 
                await connection.StopAsync();
            }
            catch(Exception ex) { 
                Console.WriteLine($"Exception during shutdown: {ex.Message}");
            }
        }

        public async Task SetupPollRequests() 
        {
            connection = new HubConnectionBuilder()
                .WithUrl(new Uri($"https://localhost:5001/deviceHub?deviceId={deviceId}"))
                .WithAutomaticReconnect()
                .Build();

            connection.On<string, string>("HeartbeatCheck", (deviceId, utcTime) =>
            {
                Console.WriteLine($"Received message: {deviceId} {utcTime}");
            });

            connection.Closed += (exception) =>
            {
                if (exception == null)
                {
                    Console.WriteLine("Connection closed without error.");
                }
                else
                {
                    Console.WriteLine($"Connection closed due to an error: {exception.Message}");
                }

                return Task.CompletedTask;
            };

            connection.Reconnected += (connectionId) =>
            {
                Console.WriteLine($"Connection successfully reconnected. The ConnectionId is now: {connectionId}");
                return Task.CompletedTask;
            };

            connection.Reconnecting += (exception) =>
            {
                Console.WriteLine($"Connection started reconnecting due to an error: {exception.Message}");
                return Task.CompletedTask;
            };

            try { 
                await connection.StartAsync();
                SetTimer();
            }
            catch(Exception ex) { 
                Console.WriteLine($"Exception during startup: {ex.Message}");
            }
        }

        private void SetTimer()
        {
            // Create a timer with a two second interval.
            aTimer = new Timer(5000);
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        private async void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            Console.WriteLine("The Elapsed event was raised at {0:HH:mm:ss.fff}",
                            e.SignalTime);
            
            if(connection.State == HubConnectionState.Connected) { 
                await connection.InvokeAsync("HeartbeatMessage", this.deviceId);
            }
            else { 
                Console.WriteLine("Not currently connected");
            }
            
        }
    }
}
