using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace SignalRServer.Hubs
{
    public enum DeviceType { 
        Manager, 
        Client
    }

    public class DeviceHub : Hub
    {
        /// <summary>
        /// Called by client devices so that the manager devices can track which 
        /// devices have recently polled.
        /// </summary>
        public async Task HeartbeatMessage(string deviceId) 
        {
            await Clients.Group("Manager").SendAsync("HeartbeatNotice", deviceId, DateTime.UtcNow);
        }

        /// <summary>
        /// Called by a client so that it can join the manager group. 
        /// </summary>
        public async Task ConnectManager(string managerId) {
            await Groups.AddToGroupAsync(Context.ConnectionId, "Manager");
            Context.Items["TestDeviceId"] = managerId;
            Context.Items["DeviceType"] = DeviceType.Manager;
            OutputContextItems(Context.Items);
        }

        // Lifecycle methods

        /// <summary>
        /// Keep a record of the different devices. 
        public override async Task OnConnectedAsync()
        {
            Context.Items["DeviceId"] = Context.GetHttpContext().Request.Query["deviceId"]; 
            Context.Items["DeviceType"] = DeviceType.Client;
            await Clients.Group("Manager").SendAsync("DeviceConnected", Context.Items["DeviceId"]);
            OutputContextItems(Context.Items);

            await base.OnConnectedAsync();
        }

        private void OutputContextItems(IDictionary<object, object> items) { 
            foreach(KeyValuePair<object, object> item in items) { 
                Console.WriteLine($"{item.Key} with value {item.Value}");
            }
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            if(((DeviceType)Context.Items["DeviceType"]) == DeviceType.Client) {
                await Clients.Group("Manager").SendAsync("DeviceDisconnected", Context.Items["DeviceId"]);
            }
            
            await base.OnDisconnectedAsync(exception);
        }

    }
}