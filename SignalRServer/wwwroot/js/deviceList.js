"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/deviceHub").build()

// Manages a list of device information
const devices = {

    items: { },

    update: function(deviceId, utcTime) { 
        this.items[deviceId] = utcTime
    }
}

connection.on("HeartbeatNotice", function(deviceId, utcTime) { 
    devices.update(deviceId, utcTime)
    updateElement(deviceId, `${deviceId} heartbeat at ${utcTime}`)
})

connection.on("DeviceConnected", function(deviceId) { 
    devices.update(deviceId, "-- connected --")
    updateElement(deviceId, `${deviceId} connected`)
})

connection.on("DeviceDisconnected", function(deviceId) { 
    devices.update(deviceId, "-- disconnected --")
    updateElement(deviceId, `${deviceId} disconnected`)
})

function updateElement(deviceId, message) { 
    let li = document.getElementById(deviceId)

    if(li == null) {
        li = document.createElement("li")
        li.setAttribute("id", deviceId)
        document.getElementById("messagesList").appendChild(li)
    }

    li.textContent = message
}

connection.start()
    .then(function () {
        connection.invoke("ConnectManager", "manager-1").catch(function (err) {
            return console.error(err.toString())
        })
    }).catch(function (err) {
        return console.error(err.toString());
    })

